package com.example.john.centerfragmentclasses;

import android.graphics.Color;

public class CenterModel {






    public String getOffer_code() {
        return offer_code;
    }

    public void setOffer_code(String offer_code) {
        this.offer_code = offer_code;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public Integer getStatusbar_color() {
        return statusbar_color;
        //return Color.BLUE;
    }

    public void setStatusbar_color(Integer statusbar_color) {
        this.statusbar_color = statusbar_color;
    }

    public String getStatusbar_text() {
        return statusbar_text;
    }

    public void setStatusbar_text(String statusbar_text) {
        this.statusbar_text = statusbar_text;
    }

    private String offer_code;
    private String last_name;
    private String first_name;
    private String room;
    private Integer statusbar_color;
    private String statusbar_text;






}
