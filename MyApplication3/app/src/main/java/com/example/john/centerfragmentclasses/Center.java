package com.example.john.centerfragmentclasses;

import android.graphics.Color;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by John on 12/22/2014.
 */
public class Center {


    public ArrayList<CenterModel> GetSearchResults(String JSONString) {


        ArrayList<CenterModel> results = new ArrayList<CenterModel>();
        CenterModel thistime = null;
        int limiter = 0;
        JSONArray fulljson = null;

        try {
            fulljson = new JSONArray(JSONString);
            Log.v("error1231",JSONString);

            for (int i = 0; i < fulljson.length(); i++) {

                String json = fulljson.getString(i);
                JSONObject object = new JSONObject(json);
                Integer color = null;
                String statusbar_text = null;

                String offercode = object.getString("offer_code");
                String lastname = object.getString("last_name");
                String firstname = object.getString("first_name");
                String room = object.getString("room_no");
                String checking_status = object.getString("checking_status");
                String last_checking = object.getString("last_checking");
                String status = object.getString("status");

                if(checking_status.equals("2/2")){
                    Log.v("statusbar",checking_status);
                    color= Color.GREEN;
                    statusbar_text = "DONE";
                }
                if(checking_status.equals("1/2")){


                    color= Color.YELLOW;
                    statusbar_text = "READY FOR TRANSACTION2";
                    //Log.v("statusbar_yellow",checking_status);
                }
                if(checking_status.equals("0/2") ){
                    Log.v("statusbar",checking_status);
                    color= Color.RED;
                    statusbar_text = "UNSAVED TRANSACTION";
                }

                if(checking_status.equals("1/2") && last_checking.equals(null) ){
                    Log.v("statusbar",checking_status);
                    color= Color.RED;
                    statusbar_text = "UNSAVED TRANSACTION2";
                }

                if(!checking_status.equals("2/2") && !status.equals("true")){
                    color= Color.RED;
                    statusbar_text = "NOT CHECKED";
                }

                if(statusbar_text==null){
                    statusbar_text="";
                    color= Color.TRANSPARENT;
                }

                Log.v("erro1122", checking_status+" "+color.toString());

                thistime = new CenterModel();
                thistime.setOffer_code(offercode);
                thistime.setLast_name(lastname + ", ");
                thistime.setFirst_name(firstname);
                thistime.setRoom(room);
                thistime.setStatusbar_color(color);
                thistime.setStatusbar_text(statusbar_text);



                results.add(thistime);



            }

        } catch (Exception e) {
            e.printStackTrace();
            Log.v("brett","error"); //comment
        }


/*        for(int j = 0; j<5; j++){
            thistime = new CenterModel();
            //thistime.setOffer_code(offercode);
            thistime.setLast_name(" ");
            thistime.setFirst_name(" ");
            thistime.setRoom(" ");
            results.add(thistime);

        }*/




        return results;
    }
}
