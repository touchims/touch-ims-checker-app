package com.example.john.webservice;

import android.util.Log;
import android.widget.Toast;

import com.example.john.myapplication.StaticFunctions;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by John on 12/23/2014.
 */
public class KonektorHTTP {
    private int ad_length= new StaticFunctions().adLength();
    private static String HOSTkonektor_session = new StaticFunctions().getHost()+"konektor_session.php";
    private static HttpClient httpclient = new DefaultHttpClient();


    public String[] postData(String url,String name,String... args) {

        HttpPost httppost = new HttpPost(url);
        String jsonresult="";
        String result[]= new String[2];
        int status;


        try {
            List<NameValuePair> values = null;
            values = phpPostName(name,args);
            httppost.setEntity(new UrlEncodedFormEntity(values));
            HttpResponse response = httpclient.execute(httppost);
            status = response.getStatusLine().getStatusCode();
            if(status!=200)
                result[1]="error";
            else
                result[1]="success";

            jsonresult = inputStreamToString(response.getEntity().getContent()).toString();
            result[0] = jsonresult.substring(0, jsonresult.length() - ad_length);

            //Log.v("test002",result[0]);
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            //Log.v("test002",e.toString());
        } catch (IOException e) {
            // TODO Auto-generated catch block
            //Log.v("test002",e.toString());
        }

        return result;

    }

    private List<NameValuePair> phpPostName(String name,String... val){

        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

        if(name=="alltime")
            nameValuePairs.add(new BasicNameValuePair("alltime","0"));
        if(name=="time")
            nameValuePairs.add(new BasicNameValuePair("time",val[0]));
        if(name=="auth"){
            nameValuePairs.add(new BasicNameValuePair("username",val[0])); // $_POST['username']
            nameValuePairs.add(new BasicNameValuePair("password",val[1]));
        }
        if(name=="offers") {
            nameValuePairs.add(new BasicNameValuePair("offercode", val[0]));

        }

        if(name=="transaction") {
            nameValuePairs.add(new BasicNameValuePair("offercode", val[0]));
            nameValuePairs.add(new BasicNameValuePair("transaction", ""));


        }
        if(name=="faculty_reports") {
            nameValuePairs.add(new BasicNameValuePair("offercode", val[0]));
            nameValuePairs.add(new BasicNameValuePair("action", val[1]));
        }
        if(name=="enable_details") {
            nameValuePairs.add(new BasicNameValuePair("action", "enable"));
            nameValuePairs.add(new BasicNameValuePair("offercode", val[0]));
            //Log.e("error005",val[1]);
        }
        if(name=="faculty_reports1"){
            nameValuePairs.add(new BasicNameValuePair("action", val[1]));
            nameValuePairs.add(new BasicNameValuePair("offercode", val[0]));
            nameValuePairs.add(new BasicNameValuePair("username", val[2]));
            Log.v("username",val[2]);
        }
        if(name=="button_labels"){
            nameValuePairs.add(new BasicNameValuePair("offercode", val[0]));
            nameValuePairs.add(new BasicNameValuePair("button_labels", ""));
        }

        if(name=="generate_sessionID"){
            nameValuePairs.add(new BasicNameValuePair("generate", val[0]));
        }

        if(name=="get_sessionID"){
            nameValuePairs.add(new BasicNameValuePair("get", val[0]));
        }

        if(name=="set_sessionID"){
            nameValuePairs.add(new BasicNameValuePair("set", val[0]));
        }

        if(name=="delete_sessionID"){
            nameValuePairs.add(new BasicNameValuePair("delete", val[0]));
        }
        if(name=="timebetween"){
            nameValuePairs.add(new BasicNameValuePair("timebetween", ""));

        }

        if(name=="sync"){
            nameValuePairs.add(new BasicNameValuePair("sync", ""));

        }

        return nameValuePairs;

    }

    private StringBuilder inputStreamToString(InputStream is) {
        String rLine = "";
        StringBuilder answer = new StringBuilder();
        BufferedReader rd = new BufferedReader(new InputStreamReader(is));

        try {
            while ((rLine = rd.readLine()) != null) {
                answer.append(rLine);
            }
        }

        catch (IOException e) {
            e.printStackTrace();
        }
        return answer;
    }

    public String[] session(String url,String action) {
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(url);

        //httppost.addHeader("cookie","PHPSESSID=123123;path=/");

        String session_result="";
        String result[]= new String[2];
        int status;
        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("test", "hehe"));
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = httpclient.execute(httppost);
            status = response.getStatusLine().getStatusCode();


            if(status!=200)
                result[1]="error";
            else
                result[1]="success";

            session_result = inputStreamToString(response.getEntity().getContent()).toString();
            result[0] = session_result.substring(0, session_result.length() - ad_length);

        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
        } catch (IOException e) {
            // TODO Auto-generated catch block
        }

        return result;

    }

}
