package com.example.john.fragment;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.john.centerfragmentclasses.Center;
import com.example.john.centerfragmentclasses.CenterAdapter;
import com.example.john.centerfragmentclasses.CenterModel;
import com.example.john.myapplication.StaticFunctions;
import com.example.john.myapplication.R;
import com.example.john.webservice.KonektorHTTP;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class CenterFragment extends Fragment {
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private String PREFNAME = "MyPref";
    private static String host = new StaticFunctions().getHost();
    private static KonektorHTTP konektorweb = new KonektorHTTP();
    private static String time;
    private boolean isWithArguments = false;
    private boolean onload = false;
    private static Bundle timebundle;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.center_fragment, container, false);


        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        pref = getActivity().getSharedPreferences(PREFNAME, 0);
        editor = pref.edit();

        try {
            timebundle = this.getArguments();
            time = timebundle.getString("time");
            isWithArguments = true;
        } catch (Exception e) {
            isWithArguments = false;
        }
        Log.v("args111", isWithArguments + "");

        if (isWithArguments)
            getSubjectThread(time);
        else
            new TimePickerTask().execute();


    }

    private void spinnerValues(String alltime, final String target_time) {
        List<String> spinnerArray = new ArrayList<String>();
        //spinnerArray.add("item1");
        //Log.v("alltime4", alltime);
        int spinneritemscount = 0;

        try {
            JSONArray jsonarray = new JSONArray(alltime);
            spinneritemscount = jsonarray.length();

            for (int i = 0; i < jsonarray.length(); i++) {
                String alltimejson = jsonarray.getString(i);
                JSONObject object = new JSONObject(alltimejson);
                String time = object.getString("time");
                spinnerArray.add(time);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(
                getActivity(), android.R.layout.simple_spinner_item, spinnerArray);

        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Spinner sItems = (Spinner) getActivity().findViewById(R.id.spinner);
        sItems.setAdapter(adapter2);


        for (int i = 0; i < spinneritemscount; i++) {
            String item = sItems.getItemAtPosition(i).toString();
            if (item.equals(target_time))
                sItems.setSelection(i);
        }


        sItems.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView adapter, View v, int i, long lng) {
                //Log.v("error111", "wew");
//
                String spinner_time = adapter.getSelectedItem().toString();
                if (!spinner_time.equals(target_time)) {
                    Bundle bundle = new Bundle();
                    bundle.putString("time", spinner_time);
                    CenterFragment centerfragment = new CenterFragment();
                    centerfragment.setArguments(bundle);
                    getFragmentManager().beginTransaction().detach(centerfragment);
                    getFragmentManager().beginTransaction().replace(R.id.center_container, centerfragment).commit();
                }


            }

            @Override
            public void onNothingSelected(AdapterView arg0) {
                Toast.makeText(getActivity(), "Nothing selected", Toast.LENGTH_SHORT).show();

            }
        });


    }


    public class GetClassesTask extends AsyncTask<String, Void, ArrayList<NameValuePair>> {
        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected ArrayList<NameValuePair> doInBackground(final String... params) {
            Log.v("tracer1", "getClassesTask" + params[0]);
            final ArrayList<NameValuePair> myList = new ArrayList<NameValuePair>();
            String hostkonektor = host + "subject_schedule.php";
            String hostgetday = host + "getday.php";
            String hostsync = host + "sync.php";
            String[] fromkonektor;// = new String[2];
            String[] fromgetday;// = new String[2];
            String[] fromsync;
            String[] fromtime;

            String time = null;
            fromkonektor = konektorweb.postData(hostkonektor, "time", params[0]);
            fromgetday = konektorweb.postData(hostgetday, null, null);
            fromsync = konektorweb.postData(hostsync, "sync", null);
            fromtime = konektorweb.postData(host + "subject_schedule.php", "alltime", "");
            time = params[0];

            final String Time = time;


            myList.add(new BasicNameValuePair("classes_where_time", fromkonektor[0]));
            myList.add(new BasicNameValuePair("classes_where_time_status", fromkonektor[1]));
            myList.add(new BasicNameValuePair("getday", fromgetday[0]));
            myList.add(new BasicNameValuePair("getday_status", fromgetday[1]));
            myList.add(new BasicNameValuePair("timedisplay", Time));
            myList.add(new BasicNameValuePair("sync_id", fromsync[0]));
            myList.add(new BasicNameValuePair("alltime", fromtime[0]));
            //myList.add(new BasicNameValuePair("target_time", fromtime[0]));

            return myList;
            //return null;
        }

        @Override
        protected void onPostExecute(ArrayList<NameValuePair> listresult) {


            try {
                pref = getActivity().getSharedPreferences(PREFNAME, 0);
            } catch (Exception e) {
                Log.v("pref", "pref");
            }
            editor = pref.edit();
            String current_sync_id = listresult.get(5).getValue();
            String saved_sync_id = pref.getString("sync_id", "");
            String classes_json = listresult.get(0).getValue();
            String today = listresult.get(2).getValue();
            final String target_time = listresult.get(4).getValue();
            String alltime = listresult.get(6).getValue();
            //Log.v("alltime2",alltime);
            //boolean sync_status = pref.getBoolean("sync_status",);
            //String sync_status = pref.getString("sync_status","");


            final ArrayList<CenterModel> offers = new Center().GetSearchResults(classes_json);

            try {
                final GridView listview = (GridView) getView().findViewById(R.id.center_listview);


                if (!onload) {
                    listview.setAdapter(new CenterAdapter(getActivity(), offers));
                    onload = true;
                    spinnerValues(alltime, target_time);
                    //Log.v("alltime3",alltime);
                    TextView dayoftheweek = (TextView) getView().findViewById(R.id.daytoday);
                    dayoftheweek.setText(today + " " + target_time);

                }

                if (current_sync_id.equals(saved_sync_id)) {
                    Log.v("sync", "equal");
                    //listview.setAdapter(new CenterAdapter(getActivity(), offers));
                }

                if (!current_sync_id.equals(saved_sync_id)) {

                    Log.v("sync", "sync id changed");

                    listview.setAdapter(new CenterAdapter(getActivity(), offers));
                    //testadapter.notifyAll();
                    //new CenterAdapter(getActivity(),offers).notifyAll();
                    editor.putString("sync_id", current_sync_id);
                    editor.commit();
                    TextView dayoftheweek = (TextView) getView().findViewById(R.id.daytoday);
                    dayoftheweek.setText(today + " " + target_time);
                    //
                }


                listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                        CenterAdapter ca = new CenterAdapter(this, offers);
                        String offercode = ca.getOC(position);
                        Bundle b = new Bundle();
                        b.putString("offer_code_centerfragment", offercode);
                        b.putString("target_time", target_time);
                        DFragment df = new DFragment();
                        df.setArguments(b);
                        FragmentManager fm = getFragmentManager();
                        df.setRetainInstance(true);
                        df.show(fm, "fragment_name");
                    }
                });
            } catch (Exception e) {
                Log.v("brett","brett");

            }

        }


    }

    public class TimePickerTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            //Log.v("error009", "test");
            String timebetween_url = host + "timebetween.php";
            String timebetween_result[] = konektorweb.postData(timebetween_url, "timebetween", "timebetween");
            return timebetween_result[0];
        }

        @Override
        protected void onPostExecute(final String result) {
            //time = result;
            Log.v("error009", "test");
            //new GetClassesTask().execute(result);
            try {
                getSubjectThread(result);
            } catch (Exception e) {
                Log.v("error888", e.toString());
            }
        }
    }

    private void getSubjectThread(final String time) {
        Log.v("getSubject", "getSubject " + time);

        Thread t = null;
        t = new Thread() {
            @Override
            public void run() {

                try {
                    final GetClassesTask gc = new GetClassesTask();
                    while (!isInterrupted()) {
                        Thread.sleep(3000);

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                String status = gc.getStatus().toString();
                                GetClassesTask gc = new GetClassesTask();

                                if (status.equals("PENDING"))
                                    gc.execute(time);

                                else {
                                    //gc = new GetClassesTask();
                                }


                            }
                        });
                    }
                } catch (Exception e) {

                    this.interrupt();
                }
            }
        };


        t.start();


    }

    public class SyncIDTask extends AsyncTask<String, Void, String[]> {
        @Override
        protected String[] doInBackground(String... params) {


            String hostsync = host + "sync.php";
            String[] fromsync;
            fromsync = konektorweb.postData(hostsync, "sync", null);
            return fromsync;
        }

        @Override
        protected void onPostExecute(String[] get_sync_id) {


            if (isWithArguments)
                getSubjectThread(time);
            else
                new TimePickerTask().execute();


        }
    }


}

//


