package com.example.john.fragment;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.john.myapplication.R;
import com.example.john.myapplication.StaticFunctions;
import com.example.john.webservice.KonektorHTTP;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class DFragment extends DialogFragment {
    private static String HOST = new StaticFunctions().getHost();
    private static String DIALOG_TEXTS = "dialog_texts";
    private static String OFFERS_FILE = "offers.php";
    private static String FACULTY_REPORTS_FILE = "faculty_reports1.php";
    private static String CHECKMARK = "CHECKMARK";
    private static String XMARK = "XMARK";
    private static String SAVE = "SAVE";
    private ImageButton checkmark_btn;
    private ImageButton xmark_btn;
    private Button save;
    private KonektorHTTP konektorweb;
    private TextView xmark_tview;
    private TextView checkmark_tview;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private String PREFNAME = "MyPref";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.checker_dialog, container,
                false);
        getDialog().setTitle("Dialog");
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);
        Bundle b = this.getArguments();
        final String offercode_argument = b.getString("offer_code_centerfragment");
        final String target_time = b.getString("target_time");
        new Konektor().execute(offercode_argument);
        checkmark_btn = (ImageButton) getView().findViewById(R.id.checkmark);
        xmark_btn = (ImageButton) getView().findViewById(R.id.xmark);
        save = (Button) getView().findViewById(R.id.save);
        checkmark_tview = (TextView) getView().findViewById(R.id.label_checkmark);
        xmark_tview = (TextView) getView().findViewById(R.id.label_xmark);

        pref = getActivity().getSharedPreferences(PREFNAME, 0);
        editor = pref.edit();
        final String login = pref.getString("login", "");


        checkmark_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new actionTask().execute(offercode_argument, CHECKMARK, login);
                checkmark_tview.setBackgroundColor(Color.BLUE);
                xmark_tview.setBackgroundColor(Color.TRANSPARENT);
                editor.putString("labelcolor","checkmark");
                editor.commit();
                save.setEnabled(true);

            }
        });

        xmark_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new actionTask().execute(offercode_argument, XMARK, login);
                checkmark_tview.setBackgroundColor(Color.TRANSPARENT);
                xmark_tview.setBackgroundColor(Color.BLUE);
                editor.putString("labelcolor","xmark");
                editor.commit();
                save.setEnabled(true);
            }
        });
        editor.putString("labelcolor","");
        editor.commit();

//        if(pref.getString("labelcolor","").equals("checkmark")){
//            checkmark_tview.setBackgroundColor(Color.BLUE);
//            xmark_tview.setBackgroundColor(Color.TRANSPARENT);
//            save.setEnabled(true);
//        }
//        if(pref.getString("labelcolor","").equals("xmark")){
//            checkmark_tview.setBackgroundColor(Color.TRANSPARENT);
//            xmark_tview.setBackgroundColor(Color.BLUE);
//            save.setEnabled(true);
//        }
//        if(pref.getString("labelcolor","").equals("")){
//            save.setEnabled(false);
//        }

        if(!pref.getString("labelcolor","").equals("")){
            //Toast.makeText(getActivity(),"selected",Toast.LENGTH_SHORT).show();
            save.setEnabled(true);
        }
        else{
            //Toast.makeText(getActivity(),"disabled",Toast.LENGTH_SHORT).show();
            save.setEnabled(false);
        }


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new actionTask().execute(offercode_argument, SAVE, login);

            }
        });


    }

    private void disabler() {

        checkmark_btn = (ImageButton) getView().findViewById(R.id.checkmark);
        xmark_btn = (ImageButton) getView().findViewById(R.id.xmark);
        save = (Button) getView().findViewById(R.id.save);
        checkmark_tview = (TextView) getView().findViewById(R.id.label_checkmark);
        xmark_tview = (TextView) getView().findViewById(R.id.label_xmark);
        checkmark_btn.setEnabled(false);
        xmark_btn.setEnabled(false);
        save.setEnabled(false);
        checkmark_btn.setImageResource(R.drawable.checkmark_gray);
        xmark_btn.setImageResource(R.drawable.xmark_gray);
        checkmark_tview.setVisibility(View.INVISIBLE);
        xmark_tview.setVisibility(View.INVISIBLE);


    }

    public class Konektor extends AsyncTask<String, Void, ArrayList<NameValuePair>> {
        @Override
        protected ArrayList<NameValuePair> doInBackground(String... params) {
            ArrayList<NameValuePair> myList = new ArrayList<NameValuePair>();
            String offercode = params[0];
            String hostoffers = HOST + OFFERS_FILE;
            String hostreports = HOST + FACULTY_REPORTS_FILE;
            String fromkonektor[] = new String[2];
            String enable_checking[] = new String[2];
            String button_labels[] = new String[2];
            String transaction[] = new String[2];
            KonektorHTTP konektorweb = new KonektorHTTP();
            try {

                fromkonektor = konektorweb.postData(hostoffers, "offers", offercode);
                enable_checking = konektorweb.postData(hostreports, "enable_details", offercode);
                button_labels = konektorweb.postData(hostreports, "button_labels", offercode);
                transaction = konektorweb.postData(hostoffers, "transaction", offercode);


            } catch (Exception e) {
            }
            myList.add(new BasicNameValuePair("offer_details", fromkonektor[0]));
            myList.add(new BasicNameValuePair("enable_details", enable_checking[0]));
            myList.add(new BasicNameValuePair("button_labels", button_labels[0]));
            myList.add(new BasicNameValuePair("transaction", transaction[0]));

            Log.v("error1243",button_labels[0]+" "+offercode);

            return myList;
        }

        @Override
        protected void onPostExecute(ArrayList<NameValuePair> listresult) {

            String label_json = listresult.get(2).getValue();
            String dialog_details_json = listresult.get(0).getValue();
            String transaction_details_json = listresult.get(3).getValue();
            //Log.v("error001", dialog_details_json);
            setDialogTexts(dialog_details_json);
            buttonLabels(label_json);
            setTransactions(transaction_details_json);
            String enabler_result = listresult.get(1).getValue();
            //Toast.makeText(getActivity(), enabler_result + "", Toast.LENGTH_SHORT).show();
            Log.v("en001",enabler_result);
            if (!enabler_result.contains("enabled"))
                disabler();


        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }

        private void setTransactions(String transaction_result) {

           //Log.v("wew123", transaction_result);

            Boolean correct = false;
            JSONObject json_object;
            JSONObject json_object_label;

            TextView status1 = (TextView) getView().findViewById(R.id.status1);
            TextView status2 = (TextView) getView().findViewById(R.id.status2);

            try {
                JSONArray fulljson = new JSONArray(transaction_result);

                String json = fulljson.getString(0);
                json_object = new JSONObject(json);

                String checking_status = json_object.getString("checking_status");

                if (checking_status.equals("2/2")) {
                    status1.setText("Transaction 1:" + json_object.getString("first_checking"));
                    status2.setText("Transaction 2:" + json_object.getString("last_checking"));
                }

                if (checking_status.equals("1/2")) {
                    status1.setText("Transaction 1:" + json_object.getString("first_checking"));
                    status2.setText("Transaction 2:" + json_object.getString(""));
//
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }


        }

        private void buttonLabels(String jsonresult){
            Log.v("en002",jsonresult);
            TextView label_checkmark = (TextView)getView().findViewById(R.id.label_checkmark);
            TextView label_xmark = (TextView)getView().findViewById(R.id.label_xmark);

            try {
                JSONObject object = new JSONObject(jsonresult);
                String checkmark = object.getString("checkmark");
                String xmark = object.getString("xmark");
                if(checkmark.equals("LOCK") && checkmark.equals("LOCK")){
                    disabler();

                }
                else {
                    label_checkmark.setText(checkmark);
                    label_xmark.setText(xmark);
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        private void setDialogTexts(String jsonresult) {

            try {
                JSONArray jsonarray = new JSONArray(jsonresult);
                String fulljson = jsonarray.getString(0);
                JSONObject object = new JSONObject(fulljson);

                String d_day = object.getString("day");
                String d_offercode = object.getString("offer_code");
                String d_time = object.getString("time");
                String d_room = object.getString("room_no");
                String d_lastname = object.getString("last_name");
                String d_firstname = object.getString("first_name");
                String d_subjname = object.getString("subj_name");

                TextView day = (TextView)getView().findViewById(R.id.d_day);
                TextView offercode = (TextView)getView().findViewById(R.id.d_offercode);
                TextView time =(TextView)getView().findViewById(R.id.d_time);
                TextView room = (TextView)getView().findViewById(R.id.d_room);
                TextView lastname = (TextView)getView().findViewById(R.id.d_lastname);
                TextView firstname = (TextView)getView().findViewById(R.id.d_firstname);
                TextView subjname = (TextView)getView().findViewById(R.id.d_courseno);

                day.setText(d_day);
                offercode.setText(d_offercode);
                time.setText(d_time);
                room.setText(d_room);
                lastname.setText(d_lastname);
                firstname.setText(d_firstname);
                subjname.setText(d_subjname);



            } catch (JSONException e) {
                e.printStackTrace();
            }




        }


    }

    public class actionTask extends AsyncTask<String, Void, String[]> {

        @Override
        protected String[] doInBackground(String... params) {
            String hostreports = HOST + FACULTY_REPORTS_FILE;
            String offercode = params[0];
            String action = params[1];
            String checker = params[2];
            String faculty_report_result[];
            konektorweb = new KonektorHTTP();
            faculty_report_result = konektorweb.postData(hostreports, "faculty_reports1", offercode, action, checker);
            return faculty_report_result;
        }

        @Override
        protected void onPostExecute(String[] result) {
            //String toast = result[0].substring(0,14);
            //Toast.makeText(getActivity(),toast, Toast.LENGTH_SHORT).show();
            Log.v("error123",result[0]);
            if (result[0].contains("DONE") || result[0].contains("Error") ||result[0].contains("PENDING") ) {
                disabler();
                getDialog().dismiss();
            }


        }

    }


}
