package com.example.john.centerfragmentclasses;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.john.myapplication.R;

import java.util.ArrayList;

/**
 * Created by John on 12/22/2014.
 */
public class CenterAdapter extends BaseAdapter {

    private int selectedItem = -1;
    private static ArrayList<CenterModel> time_arraylist;
    private LayoutInflater l_Inflater;

    public CenterAdapter(Context context, ArrayList<CenterModel> results) {
        time_arraylist = results;
        l_Inflater = LayoutInflater.from(context);
    }

    public CenterAdapter(Object context, Object results) {
        // TODO Auto-generated constructor stub
    }

    public int getCount() {
        return time_arraylist.size();
    }

    public Object getItem(int position) {

        return time_arraylist.get(position);
    }

    public String getOC(int position){
        return time_arraylist.get(position).getOffer_code();
    }

/*    public String getItemString(int position){
        return time_arraylist.get(position).getTime();
    }*/



    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = l_Inflater.inflate(R.layout.center_item, null);
            holder = new ViewHolder();
            //holder.offer_code = (TextView) convertView.findViewById(R.id.offer_code);
            holder.last_name = (TextView) convertView.findViewById(R.id.last_name);
            holder.first_name = (TextView) convertView.findViewById(R.id.first_name);
            holder.room = (TextView) convertView.findViewById(R.id.room);
            holder.status_bar = (TextView) convertView.findViewById(R.id.statusbar);




            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        String fname_initial = (time_arraylist.get(position).getFirst_name());
        fname_initial = fname_initial.substring(0,1);
        //holder.offer_code.setText(time_arraylist.get(position).getOffer_code());
        holder.last_name.setText(time_arraylist.get(position).getLast_name()+fname_initial);
        holder.first_name.setText(fname_initial);
        holder.room.setText(time_arraylist.get(position).getRoom());
        holder.status_bar.setBackgroundColor(time_arraylist.get(position).getStatusbar_color());
        holder.status_bar.setText(time_arraylist.get(position).getStatusbar_text());
        //holder.room.setTag(time_arraylist.get(position).getOffer_code());




        return convertView;
    }



    static class ViewHolder {
        TextView offer_code;
        TextView last_name;
        TextView first_name;
        TextView room;
        TextView status_bar;


    }
}
