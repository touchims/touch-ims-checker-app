package com.example.john.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import com.example.john.myapplication.MainActivity;
import com.example.john.myapplication.R;

public class HeaderFragment extends Fragment {

    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private LoginFragment loginfragment;
    private SideFragment sidefragment;
    private HeaderFragment headerfragment;
    private CenterFragment centerfragment;
    private String PREFNAME="MyPref";
    private FragmentManager fm;
    private FragmentTransaction ft;
    private TextView fnameview;
    private TextView lnameview;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.header_fragment, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        pref = getActivity().getSharedPreferences(PREFNAME, 0);
        editor = pref.edit();
        ((MainActivity)getActivity()).lockDrawer(false);
        String full_name = pref.getString("login","");

        fnameview = (TextView)getView().findViewById(R.id.header_firstname);
        lnameview = (TextView)getView().findViewById(R.id.header_lastname);

        fnameview.setText(full_name);
        lnameview.setText("");

        ImageButton logout_btn = (ImageButton)getView().findViewById(R.id.logout_btn);
        Button refresh_btn = (Button)getView().findViewById(R.id.refresh_btn);

        logout_btn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                loginfragment = new LoginFragment();
                sidefragment = new SideFragment();
                headerfragment = new HeaderFragment();
                centerfragment = new CenterFragment();
                editor.clear();
                editor.commit();
                fm = getFragmentManager();
                ft = fm.beginTransaction();
                ft.replace(R.id.header_container,headerfragment);
                ft.replace(R.id.side_container,sidefragment);
                ft.replace(R.id.center_container,centerfragment);
                ft.remove(headerfragment);
                ft.remove(sidefragment);
                ft.remove(centerfragment);
                ft.replace(R.id.login_container, loginfragment);
                ft.commit();
            }
        });

        refresh_btn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                loginfragment = new LoginFragment();
                sidefragment = new SideFragment();
                headerfragment = new HeaderFragment();
                centerfragment = new CenterFragment();
                editor.clear();
                editor.commit();
                fm = getFragmentManager();
                ft = fm.beginTransaction();
                //ft.replace(R.id.header_container,headerfragment);
                //ft.replace(R.id.side_container,sidefragment);
                ft.replace(R.id.center_container,centerfragment);
                //ft.remove(headerfragment);
                //ft.remove(sidefragment);
                ft.remove(centerfragment);
                //ft.replace(R.id.login_container, loginfragment);
                ft.replace(R.id.center_container,centerfragment);
                ft.commit();
            }
        });




    }


}