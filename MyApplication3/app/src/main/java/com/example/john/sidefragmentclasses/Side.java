package com.example.john.sidefragmentclasses;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by John on 12/22/2014.
 */
public class Side {



    public ArrayList<SideModel> GetSearchResults(String JSONString){

        //Log.e("timepicker",JSONString);
        ArrayList<SideModel> results = new ArrayList<SideModel>();
        SideModel time;
        try {
            JSONArray fulljson = new JSONArray(JSONString);
            for(int i = 0; i<fulljson.length();i++) {

                String json = fulljson.getString(i);
                JSONObject object = new JSONObject(json);
                String json2 = object.getString("time");

                time = new SideModel();
                time.setTime(json2);
                results.add(time);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }



        return results;
    }


}
